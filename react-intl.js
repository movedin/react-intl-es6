'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});
exports.__addLocaleData = __addLocaleData;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _intlMessageformat = require('intl-messageformat');

var _intlMessageformat2 = _interopRequireDefault(_intlMessageformat);

var _intlRelativeformat = require('intl-relativeformat');

var _intlRelativeformat2 = _interopRequireDefault(_intlRelativeformat);

var _en = require('./en');

var _en2 = _interopRequireDefault(_en);

var _componentsFormattedDate = require('./components/FormattedDate');

var _componentsFormattedDate2 = _interopRequireDefault(_componentsFormattedDate);

exports.FormattedDate = _componentsFormattedDate2['default'];

var _componentsFormattedTime = require('./components/FormattedTime');

var _componentsFormattedTime2 = _interopRequireDefault(_componentsFormattedTime);

exports.FormattedTime = _componentsFormattedTime2['default'];

var _componentsFormattedRelative = require('./components/FormattedRelative');

var _componentsFormattedRelative2 = _interopRequireDefault(_componentsFormattedRelative);

exports.FormattedRelative = _componentsFormattedRelative2['default'];

var _componentsFormattedNumber = require('./components/FormattedNumber');

var _componentsFormattedNumber2 = _interopRequireDefault(_componentsFormattedNumber);

exports.FormattedNumber = _componentsFormattedNumber2['default'];

var _componentsFormattedMessage = require('./components/FormattedMessage');

var _componentsFormattedMessage2 = _interopRequireDefault(_componentsFormattedMessage);

exports.FormattedMessage = _componentsFormattedMessage2['default'];

var _componentsFormattedHTMLMessage = require('./components/FormattedHTMLMessage');

var _componentsFormattedHTMLMessage2 = _interopRequireDefault(_componentsFormattedHTMLMessage);

exports.FormattedHTMLMessage = _componentsFormattedHTMLMessage2['default'];

var _Intl2 = require('./Intl');

var _Intl3 = _interopRequireDefault(_Intl2);

exports.Intl = _Intl3['default'];

function __addLocaleData(data) {
    _intlMessageformat2['default'].__addLocaleData(data);
    _intlRelativeformat2['default'].__addLocaleData(data);
}

__addLocaleData(_en2['default']);