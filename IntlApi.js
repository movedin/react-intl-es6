'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _intlMessageformat = require('intl-messageformat');

var _intlMessageformat2 = _interopRequireDefault(_intlMessageformat);

var _intlRelativeformat = require('intl-relativeformat');

var _intlRelativeformat2 = _interopRequireDefault(_intlRelativeformat);

var _intlFormatCache = require('intl-format-cache');

var _intlFormatCache2 = _interopRequireDefault(_intlFormatCache);

/**
 * Determine if the `date` is valid by checking if it is finite, which is
 * the same way that `Intl.DateTimeFormat#format()` checks.
 * @private
 */
function assertIsDate(date, errMsg) {
    if (!isFinite(date)) {
        throw new TypeError(errMsg);
    }
}

/**
 *
 * This is the Intl API that should be passed as a context throughout the application.
 * Every component from this package uses this.
 *
 */

var IntlApi = (function () {
    function IntlApi(locales, messages, formats) {
        _classCallCheck(this, IntlApi);

        this.messages = messages;
        this.formats = formats;
        this.locales = locales;

        this.getNumberFormat = (0, _intlFormatCache2['default'])(Intl.NumberFormat);
        this.getDateTimeFormat = (0, _intlFormatCache2['default'])(Intl.DateTimeFormat);
        this.getMessageFormat = (0, _intlFormatCache2['default'])(_intlMessageformat2['default']);
        this.getRelativeFormat = (0, _intlFormatCache2['default'])(_intlRelativeformat2['default']);
    }

    _createClass(IntlApi, [{
        key: 'formatDate',
        value: function formatDate(date, options) {
            date = new Date(date);
            assertIsDate(date, 'A date or timestamp must be provided to formatDate()');
            return this._format('date', date, options);
        }
    }, {
        key: 'formatTime',
        value: function formatTime(date, options) {
            date = new Date(date);
            assertIsDate(date, 'A date or timestamp must be provided to formatTime()');
            return this._format('time', date, options);
        }
    }, {
        key: 'formatRelative',
        value: function formatRelative(date, options, formatOptions) {
            date = new Date(date);
            assertIsDate(date, 'A date or timestamp must be provided to formatRelative()');
            return this._format('relative', date, options, formatOptions);
        }
    }, {
        key: 'formatNumber',
        value: function formatNumber(num, options) {
            return this._format('number', num, options);
        }
    }, {
        key: 'formatMessage',
        value: function formatMessage(message, values) {
            // When `message` is a function, assume it's an IntlMessageFormat
            // instance's `format()` method passed by reference, and call it. This
            // is possible because its `this` will be pre-bound to the instance.
            if (typeof message === 'function') {
                return message(values);
            }

            if (typeof message === 'string') {
                message = this.getMessageFormat(message, this.locales, this.formats);
            }

            return message.format(values);
        }
    }, {
        key: 'getMessage',
        value: function getMessage(path) {
            var messages = this.messages;
            var pathParts = path.split('.');

            var message;

            try {
                message = pathParts.reduce(function (obj, pathPart) {
                    return obj[pathPart];
                }, messages);
            } finally {
                if (message === undefined) {
                    throw new ReferenceError('Could not find Intl message: ' + path);
                }
            }

            return message;
        }
    }, {
        key: 'getFormat',
        value: function getFormat(type, format) {
            if (typeof format === 'string') {
                return this.getNamedFormat(type, format);
            } else {
                return format;
            }
        }
    }, {
        key: 'getNamedFormat',
        value: function getNamedFormat(type, name) {
            var formats = this.formats;
            var format = null;

            try {
                format = formats[type][name];
            } finally {
                if (!format) {
                    throw new ReferenceError('No ' + type + ' format named: ' + name);
                }
            }

            return format;
        }
    }, {
        key: '_format',
        value: function _format(type, value, options, formatOptions) {
            var locales = this.locales;

            if (options && typeof options === 'string') {
                options = this.getNamedFormat(type, options);
            }

            switch (type) {
                case 'date':
                case 'time':
                    return this.getDateTimeFormat(locales, options).format(value);
                case 'number':
                    return this.getNumberFormat(locales, options).format(value);
                case 'relative':
                    return this.getRelativeFormat(locales, options).format(value, formatOptions);
                default:
                    throw new Error('Unrecognized format type: ' + type);
            }
        }
    }]);

    return IntlApi;
})();

exports['default'] = IntlApi;
;
module.exports = exports['default'];