'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _reactIntl = require('./react-intl');

Object.defineProperty(exports, 'Intl', {
    enumerable: true,
    get: function get() {
        return _reactIntl.Intl;
    }
});
Object.defineProperty(exports, 'IntlApi', {
    enumerable: true,
    get: function get() {
        return _reactIntl.IntlApi;
    }
});
Object.defineProperty(exports, 'FormattedDate', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedDate;
    }
});
Object.defineProperty(exports, 'FormattedTime', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedTime;
    }
});
Object.defineProperty(exports, 'FormattedRelative', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedRelative;
    }
});
Object.defineProperty(exports, 'FormattedNumber', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedNumber;
    }
});
Object.defineProperty(exports, 'FormattedMessage', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedMessage;
    }
});
Object.defineProperty(exports, 'FormattedHTMLMessage', {
    enumerable: true,
    get: function get() {
        return _reactIntl.FormattedHTMLMessage;
    }
});
Object.defineProperty(exports, '__addLocaleData', {
    enumerable: true,
    get: function get() {
        return _reactIntl.__addLocaleData;
    }
});