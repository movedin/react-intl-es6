'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var FormattedMessage = (function (_Component) {
    _inherits(FormattedMessage, _Component);

    function FormattedMessage() {
        _classCallCheck(this, FormattedMessage);

        _get(Object.getPrototypeOf(FormattedMessage.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(FormattedMessage, [{
        key: 'render',
        value: function render() {
            var props = this.props;
            var _props = this.props;
            var tagName = _props.tagName;
            var message = _props.message;
            var className = _props.className;

            // Creates a token with a random guid that should not be guessable or
            // conflict with other parts of the `message` string.
            var guid = Math.floor(Math.random() * 0x10000000000).toString(16);
            var tokenRegex = new RegExp('(@__ELEMENT-' + guid + '-\\d+__@)', 'g');
            var elements = {};

            var generateToken = (function () {
                var counter = 0;
                return function () {
                    return '@__ELEMENT-' + guid + '-' + (counter += 1) + '__@';
                };
            })();

            // Iterates over the `props` to keep track of any React Element values
            // so they can be represented by the `token` as a placeholder when the
            // `message` is formatted. This allows the formatted message to then be
            // broken-up into parts with references to the React Elements inserted
            // back in.
            var values = Object.keys(props).reduce(function (values, name) {
                var value = props[name];
                var token;

                if (_react2['default'].isValidElement(value)) {
                    token = generateToken();
                    values[name] = token;
                    elements[token] = value;
                } else {
                    values[name] = value;
                }

                return values;
            }, {});

            // Formats the `message` with the `values`, including the `token`
            // placeholders for React Element values.
            var formattedMessage = this.context.intl.formatMessage(message, values);

            // Split the message into parts so the React Element values captured
            // above can be inserted back into the rendered message. This
            // approach allows messages to render with React Elements while keeping
            // React's virtual diffing working properly.
            var children = formattedMessage.split(tokenRegex).filter(function (part) {
                // Ignore empty string parts.
                return !!part;
            }).map(function (part) {
                // When the `part` is a token, get a ref to the React Element.
                return elements[part] || part;
            });

            return _react2['default'].createElement.apply(_react2['default'], [tagName, { className: className }].concat(_toConsumableArray(children)));
        }
    }], [{
        key: 'contextTypes',
        value: {
            intl: _react2['default'].PropTypes.object
        },
        enumerable: true
    }, {
        key: 'propTypes',
        value: {
            tagName: _react2['default'].PropTypes.string,
            message: _react2['default'].PropTypes.string.isRequired
        },
        enumerable: true
    }, {
        key: 'defaultProps',
        value: {
            tagName: 'span'
        },
        enumerable: true
    }]);

    return FormattedMessage;
})(_react.Component);

exports['default'] = FormattedMessage;
module.exports = exports['default'];