'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _escape = require('./escape');

var _escape2 = _interopRequireDefault(_escape);

var FormattedHTMLMessage = (function (_Component) {
    _inherits(FormattedHTMLMessage, _Component);

    function FormattedHTMLMessage() {
        _classCallCheck(this, FormattedHTMLMessage);

        _get(Object.getPrototypeOf(FormattedHTMLMessage.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(FormattedHTMLMessage, [{
        key: 'render',
        value: function render() {
            var _this = this;

            var _props = this.props;
            var tagName = _props.tagName;
            var message = _props.message;
            var className = _props.className;

            // Process all the props before they are used as values when formatting
            // the ICU Message string. Since the formatted message will be injected
            // via `innerHTML`, all String-based values need to be HTML-escaped. Any
            // React Elements that are passed as props will be rendered to a static
            // markup string that is presumed to be safe.
            var values = Object.keys(this.props).reduce(function (values, name) {
                var value = _this.props[name];

                if (typeof value === 'string') {
                    value = (0, _escape2['default'])(value);
                } else if (_react2['default'].isValidElement(value)) {
                    value = _react2['default'].renderToStaticMarkup(value);
                }

                values[name] = value;
                return values;
            }, {});

            // Since the message presumably has HTML in it, we need to set
            // `innerHTML` in order for it to be rendered and not escaped by React.
            // To be safe, all string prop values were escaped before formatting the
            // message. It is assumed that the message is not UGC, and came from
            // the developer making it more like a template.
            //
            // Note: There's a perf impact of using this component since there's no
            // way for React to do its virtual DOM diffing.
            return _react2['default'].createElement(tagName, {
                className: className,
                dangerouslySetInnerHTML: {
                    __html: this.context.intl.formatMessage(message, values)
                }
            });
        }
    }], [{
        key: 'contextTypes',
        value: {
            intl: _react2['default'].PropTypes.object
        },
        enumerable: true
    }, {
        key: 'propTypes',
        value: {
            tagName: _react2['default'].PropTypes.string,
            message: _react2['default'].PropTypes.string.isRequired
        },
        enumerable: true
    }, {
        key: 'defaultProps',
        value: {
            tagName: 'span'
        },
        enumerable: true
    }]);

    return FormattedHTMLMessage;
})(_react.Component);

exports['default'] = FormattedHTMLMessage;
module.exports = exports['default'];