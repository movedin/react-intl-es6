'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Utils = require('./Utils');

var FormattedNumber = (function (_Component) {
    _inherits(FormattedNumber, _Component);

    function FormattedNumber() {
        _classCallCheck(this, FormattedNumber);

        _get(Object.getPrototypeOf(FormattedNumber.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(FormattedNumber, [{
        key: 'render',
        value: function render() {
            var _props = this.props;
            var tagName = _props.tagName;
            var value = _props.value;
            var format = _props.format;
            var className = _props.className;

            var defaults = format && this.context.intl.getFormat('number', format);
            var options = (0, _Utils.filterFormatOptions)(this.props, FormattedNumber.formatOptions, defaults);

            return _react2['default'].createElement(tagName, { className: className }, this.context.intl.formatNumber(value, options));
        }
    }], [{
        key: 'contextTypes',
        value: {
            intl: _react2['default'].PropTypes.object
        },
        enumerable: true
    }, {
        key: 'formatOptions',
        value: ['localeMatcher', 'style', 'currency', 'currencyDisplay', 'useGrouping', 'minimumIntegerDigits', 'minimumFractionDigits', 'maximumFractionDigits', 'minimumSignificantDigits', 'maximumSignificantDigits'],
        enumerable: true
    }, {
        key: 'propTypes',
        value: {
            format: _react2['default'].PropTypes.oneOfType([_react2['default'].PropTypes.string, _react2['default'].PropTypes.object]),
            value: _react2['default'].PropTypes.any.isRequired
        },
        enumerable: true
    }, {
        key: 'defaultProps',
        value: {
            tagName: 'span'
        },
        enumerable: true
    }]);

    return FormattedNumber;
})(_react.Component);

exports['default'] = FormattedNumber;
module.exports = exports['default'];