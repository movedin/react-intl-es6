"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.filterFormatOptions = filterFormatOptions;

function filterFormatOptions(obj) {
    var formatOptions = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];
    var defaults = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

    return formatOptions.reduce(function (opts, name) {
        if (obj.hasOwnProperty(name)) {
            opts[name] = obj[name];
        } else if (defaults.hasOwnProperty(name)) {
            opts[name] = defaults[name];
        }

        return opts;
    }, {});
}